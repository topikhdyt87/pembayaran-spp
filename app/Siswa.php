<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nisn', 'nis', 'nama', 'id_kelas', 'alamat', 'nomor_telp', 'id_spp'];

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }

    public function spp() 
    {
        return $this->belongsTo(Spp::class, 'id_spp');
    }

}

<?php

namespace App\Exports;

use App\Pembayaran;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PembayaranExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view() : View 
    {
        return view('Laporan.PembayaranExel',['pembayaran' => Pembayaran::all()]);
    }
}

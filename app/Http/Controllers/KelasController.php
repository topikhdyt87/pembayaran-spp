<?php

namespace App\Http\Controllers;

use App\Kelas;
use Illuminate\Http\Request;
use Alert;

class KelasController extends Controller
{
    
    public function __construct() {
        $this->middleware([
            'auth',
            'privilege:admin'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kelas::paginate(5);
        return view('dashboard.kelas.index', compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kelas' => 'required',
            'kompetensi_keahlian' => 'required'
        ]);
        Kelas::create([
            'nama_kelas' => $request->nama_kelas,
            'kompetensi_keahlian' => $request->kompetensi_keahlian
        ]);
        Alert::success('Berhasil', 'Data Berhasil Di Tambahkan');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit(Kelas $kelas)
    {
        return view('dashboard.kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kelas $kelas)
    {
        $request->validate([
            'nama_kelas' => 'required',
            'kompetensi_keahlian' => 'required'
        ]);
        Kelas::where('id', $kelas->id)
        ->update([
            'nama_kelas' => $request->nama_kelas,
            'kompetensi_keahlian' => $request->kompetensi_keahlian
        ]);
        Alert::success('Berhasil', 'Data Berhasil Diubah');
        return redirect()->route('kelas.index');
    }

    /**
     * Remove the specified resource from storage.
     * !! Fix this, before all done 
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kelas $kelas)
    {
        $kelas = Kelas::destroy($kelas->id);
        if($kelas >= 0) {
            Alert::success('berhasil', 'Data berhasil dihapus');
            return back();
        } else {
            Alert::error('Gagal!', 'Silahkan Hubungi depeloper untuk informasi lebih lanjut');
            return back();
        }
  
    }
}

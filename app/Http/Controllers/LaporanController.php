<?php

namespace App\Http\Controllers;

use App\Exports\PembayaranExport;
use Excel;
use PDF;
use App\Pembayaran;
use Illuminate\Http\Request;


class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth', 
            'privilege:admin&petugas'
        ]);
    }

    public function index( ) 
    {
        return view('dashboard.laporan.index');
    }

    public function exportPDF() 
    {
        $pembayaran = Pembayaran::orderBy('created_at', 'DESC')->get();
        $pdf = PDF::loadView('Laporan.PembayaranPDF', compact('pembayaran'));
        return $pdf->download('Laporan-' . now() . '-pembayaran.pdf');
    }

    public function exportExel()
    {
        return Excel::download(new PembayaranExport, 'laporan-' . now()->format('d M y') . '-pembayaran.xlsx');
    }
}

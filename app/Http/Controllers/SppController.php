<?php

namespace App\Http\Controllers;

use App\Spp;
use Illuminate\Http\Request;
use Alert;

class SppController extends Controller
{
    public function __construct() 
    {
        $this->middleware([
            'auth',
            'privilege:admin'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spp = Spp::paginate(5);
        return view('dashboard.spp.index', compact('spp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun' => 'required|integer|min:4',
            'nominal' => 'required|integer'
        ]);
        Spp::create([
            'tahun' => $request->tahun,
            'nominal' => $request->nominal
        ]);
        Alert::success('Berhasil', 'Data Berhasil ditambahkan');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Spp  $spp
     * @return \Illuminate\Http\Response
     */
    public function show(Spp $spp)
    {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Spp  $spp
     * @return \Illuminate\Http\Response
     */
    public function edit(Spp $spp)
    {
        return view('dashboard.spp.edit', compact('spp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Spp  $spp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spp $spp)
    {
        $request->validate([
            'tahun' => 'required|integer|min:4',
            'nominal' => 'required|integer'
        ]);
        Spp::where('id', $spp->id)
                ->update([
                    'tahun' => $request->tahun,
                    'nominal' => $request->nominal
                ]);
        Alert::success('Berhasil', 'Data Berhasil diubah');
        return redirect()->route('spp.index');
    }

    /**
     * Remove the specified resource from storage.
     * !! Fix this, before all done 
     *
     * @param  \App\Spp  $spp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spp $spp)
    {
        $spp = Spp::destroy($spp->id);
        if($spp >= 0) {
            Alert::success('Berhasil', 'Data berhasil dihapus');
            return back();
        } else {
            Alert::error('Gagal', 'Silahkan Hubungi depeloper untuk informasi lebih lanjut');
            return back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembayaran;
use App\Siswa;
use App\Kelas;
use App\Charts\UserChart;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([
            'auth',
            'privilege:admin&petugas'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currentMonthPembayaran = Pembayaran::whereMonth('created_at', now()->month)->count();
        $lastMonthPembayaran = Pembayaran::whereMonth('created_at', now()->startOfMonth()->subMonth()->month)->count();
        $pembayaran = Pembayaran::offset(0)->limit(2)->orderBy('created_at', 'DESC')->get();
        $percentage = ($lastMonthPembayaran != 0) ? ($currentMonthPembayaran / $lastMonthPembayaran)  * 100 : $currentMonthPembayaran *100;
        $jumlahSiswa = Siswa::all()->count();
        $jumlahKelas = Kelas::all()->count();
        $usersChart = new UserChart;
        $usersChart->labels(['Bulan Lalu','Bulan Sekarang']);
        $usersChart->dataset('Data Pembayaran', 'doughnut', [$lastMonthPembayaran,$currentMonthPembayaran])
                    ->color(["rgba(255, 99, 132, 1.0)", "rgba(22,160,133, 1.0)"])
                    ->backgroundcolor(["rgba(255, 99, 132, 0.2)", "rgba(22,160,133, 0.2)"]);
        return view('dashboard.index', compact('currentMonthPembayaran', 'percentage','jumlahSiswa', 'jumlahKelas','usersChart', 'pembayaran'));
    }
}

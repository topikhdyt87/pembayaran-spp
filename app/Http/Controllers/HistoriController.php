<?php

namespace App\Http\Controllers;
use App\Pembayaran;
use Illuminate\Http\Request;

class HistoriController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'privilege:admin&petugas'
        ]);
    }

    public function index()
    {
        $pembayaran = Pembayaran::orderBy('created_at', 'DESC')->get();
        return view('dashboard.histori.index', compact('pembayaran'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Kelas;
use App\Spp;
use Illuminate\Http\Request;
use Alert;

class SiswaController extends Controller
{

    public function __construct() 
    {
        $this->middleware([
            'auth',
            'privilege:admin&petugas'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::paginate(5);
        return view('dashboard.siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $spp = Spp::all();
        $kelas = Kelas::all();
        return view('dashboard.siswa.create', compact('kelas','spp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nisn' => 'required|min:12|numeric',
            'nis' => 'required|min:10|numeric',
            'nama' => 'required',
            'id_kelas' => 'required',
            'alamat' => 'required', 
            'nomor_telp' => 'required|min:10|numeric',
            'id_spp' => 'required',
        ]);
        Siswa::create($request->all());
        Alert::success('Berhasil', 'Data Berhasil Di Tambahkan');
        return redirect()->route('siswa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Siswa $siswa)
    {
        $data = [
            'kelas' => Kelas::all(),
            'spp' => Spp::all(),
            'siswa' => $siswa
        ];
        return view('dashboard.siswa.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $request->validate([
            'nisn' => 'required|min:12|numeric',
            'nis' => 'required|min:10|numeric',
            'nama' => 'required',
            'id_kelas' => 'required',
            'alamat' => 'required', 
            'nomor_telp' => 'required|min:10|numeric',
            'id_spp' => 'required',
        ]);
        Siswa::where('id', $siswa->id)
        ->update([
            'nisn' => $request->nisn,
            'nis' => $request->nis,
            'nama' => $request->nama,
            'id_kelas' => $request->id_kelas,
            'alamat' => $request->alamat,
            'nomor_telp' => $request->nomor_telp,
            'id_spp' => $request->id_spp
        ]);
        Alert::success('Berhasil', 'Data Berhasil Diubah');
        return redirect()->route('siswa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
        $siswa = Siswa::destroy($siswa->id);
        if($siswa >= 0) {
            Alert::success('Berhasil', 'Data berhasil dihapus');
            return back();
        } else {
            Alert::error('Gagal', 'Silahkan Hubungi depeloper untuk informasi lebih lanjut');
            return back();
        }
    }
}

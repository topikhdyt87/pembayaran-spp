<?php

namespace App\Http\Controllers;

use App\Pembayaran;
use App\Siswa;
use Illuminate\Http\Request;
use Alert;

class PembayaranController extends Controller
{

    public function __construct() 
    {
        $this->middleware([
            'auth',
            'privilege:admin&petugas'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pembayaran = Pembayaran::paginate(5);
        return view('dashboard.pembayaran.index', compact('pembayaran'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('dashboard.pembayaran.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nisn' => 'required|min:12',
            'spp_bulan' => 'required',
            'jumlah_bayar' => 'required|integer', 
        ]);

        // if siswa not available ? back to previous page and send alert error
        if(Siswa::where('nisn', $request->nisn)->exists() === false) {
            Alert::error('Terjadi Kesalahan', 'Data Dengan Nisn Bersangkutan tidak tersedia');
            return back();
        }

        // if available : insert data 
        $siswa = Siswa::where('nisn', $request->nisn)->first();
        Pembayaran::create([
            'id_petugas' => auth()->user()->id,
            'id_siswa' => $siswa->id,
            'spp_bulan' => $request->spp_bulan,
            'jumlah_bayar' => $request->jumlah_bayar
        ]);

        // redirect to previous page and send alert success
        Alert::success('Berhasil', 'Data Berhasil Di Tambahkan');
        return back();

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pembayaran $pembayaran)
    {

        return view('dashboard.pembayaran.edit', compact('pembayaran'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pembayaran $pembayaran)
    {

        $request->validate([
            'nisn' => 'required|min:12',
            'spp_bulan' => 'required',
            'jumlah_bayar' => 'required|integer', 
        ]);

        // if siswa not available ? back to previous page and send alert error
        if(Siswa::where('nisn', $request->nisn)->exists() === false) {
            Alert::error('Terjadi Kesalahan', 'Data Dengan Nisn Bersangkutan tidak tersedia');
            return back();
        }

        // if available : insert data 
        $siswa = Siswa::where('nisn', $request->nisn)->first();
        Pembayaran::where('id', $pembayaran->id)
                ->update([
                    'id_petugas' => auth()->user()->id,
                    'id_siswa' => $siswa->id,
                    'spp_bulan' => $request->spp_bulan,
                    'jumlah_bayar' => $request->jumlah_bayar
                ]);

        // redirect to previous page and send alert success
        Alert::success('Berhasil', 'Data Berhasil Diubah');
        return redirect()->route('pembayaran.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembayaran $pembayaran)
    {

        $pembayaran = Pembayaran::destroy($pembayaran->id);
        if($pembayaran >= 0) {
            Alert::success('Berhasil', 'Data berhasil dihapus');
            return back();
        } else { 
            Alert::error('Gagal', 'Silahkan Hubungi depeloper untuk informasi lebih lanjut');
            return back();
        }
        
    }
}

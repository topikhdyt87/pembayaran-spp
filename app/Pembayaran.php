<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $fillable = ['id_petugas', 'id_siswa', 'spp_bulan', 'jumlah_bayar'];


    public function siswa() 
    {
        return $this->belongsTo(Siswa::class, 'id_siswa');
    }

    public function kelas() 
    {
        return $this->belongsTo(Kelas::class, 'id_kelas');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'id_petugas', 'id');
    }
}

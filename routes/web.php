<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Auth::routes();

Route::prefix('dashboard')->group(function(){

    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::resource('/spp', 'SppController');
    Route::resource('/kelas', 'KelasController', ['parameters' => ['kelas' => 'kelas']]);
    Route::resource('/siswa', 'SiswaController');
    Route::resource('/pembayaran', 'PembayaranController');
    Route::get('/laporan', 'LaporanController@index')->name('laporan');
    Route::get('/laporan/generatePDF', 'LaporanController@exportPDF')->name('generateLaporan');
    Route::get('/laporan/generateExel', 'LaporanController@exportExel')->name('generateLaporanExel');
    Route::get('/historiPembayaran', 'HistoriController@index')->name('histori');
    Route::resource('/petugas', 'PetugasController', ['parameters' => ['petugas' => 'petugas']]);

});
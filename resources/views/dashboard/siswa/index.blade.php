@extends('layouts.main')

@section('title', 'Siswa | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Data Siswa</span>
        <h3 class="page-title">Siswa</h3>
    </div>
</div>
@endsection
{{-- end header content --}}


{{-- main content --}}
@section('content')
<div class="row">

    <!-- Table Siswa -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Data Siswa</h6>
            </div>
            <div class="card-body pt-4">
                <div class="row">
                    <div class="col-12 my-2">
                        <a href="{{ route('siswa.create') }}" class="shadow btn btn-success">Tambah Data Siswa</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 overflow-auto">
                        <table class="table mb-0 text-center">
                            <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">#</th>
                                    <th scope="col" class="border-0">Nisn</th>
                                    <th scope="col" class="border-0">Nama Siswa</th>
                                    <th scope="col" class="border-0">Kelas</th>
                                    <th scope="col" class="border-0">Nomor Telp</th>
                                    <th scope="col" class="border-0">Alamat</th>
                                    <th scope="col" class="border-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($siswa as $s)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $s->nisn }}</td>
                                    <td>{{ $s->nama }}</td>
                                    <td>{{ $s->kelas->nama_kelas }}</td>
                                    <td>{{ $s->nomor_telp }}</td>
                                    <td>{{ $s->alamat }}</td>
                                    <td>
                                        <a href="{{ route('siswa.edit', ['id' => $s->id]) }}"
                                            class="btn btn-primary btn-sm">Edit</a>
                                        <form action="{{ route('siswa.destroy', ['id' => $s->id]) }}"
                                            id="formDeleteSiswa-{{ $s->id }}" class="d-inline-block" method="post">
                                            @csrf @method('delete')
                                            <button type="button" class="btn btn-danger btn-sm"
                                                onclick="deleteData({{ $s->id }})">hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div>
                    {{-- {{ $spp->links() }} --}}
                </div>
            </div>
        </div>
    </div>
    <!-- End Table Siswa -->
</div>
@endsection
@section('js')
<script>
    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeleteSiswa-' + id).submit();
            }
        })
    }
</script>
@endsection
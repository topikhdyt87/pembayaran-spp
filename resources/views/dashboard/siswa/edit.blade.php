@extends('layouts.main')

@section('title', 'Entry Pembayaran | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Edit Data Siswa</span>
        <h3 class="page-title">Data Siswa</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Edit Data Siswa</h6>
            </div>
            <div class="card-body pt-3">
                <form action="{{ route('siswa.update', ['id' => $siswa->id]) }}" method="post">@csrf @method('put')

                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" class="form-control form-control-sm @error('nisn') is-invalid @enderror"
                            id="nisn" name="nisn" value="{{ $siswa->nisn }}" autocomplete="off">
                        @error('nisn')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="nis">NIS</label>
                        <input type="number" class="form-control form-control-sm @error('nis') is-invalid @enderror"
                            id="nis" name="nis" value="{{ $siswa->nis }}" autocomplete="off">
                        @error('nis')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control form-control-sm @error('nama') is-invalid @enderror"
                            id="nama" name="nama" value="{{ $siswa->nama }}" autocomplete="off">
                        @error('nama')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="id_kelas">Nama Kelas</label>
                        <select class="form-control form-control-sm @error('id_kelas') is-invalid @enderror"
                            id="id_kelas" name="id_kelas">
                            <option value="">Nama Kelas</option>
                            @foreach ($kelas as $k)
                            @if ($siswa->id_kelas == $k->id)
                            <option value="{{ $k->id }}" selected>{{ $k->nama_kelas }}</option>
                            @else
                            <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('id_kelas')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control form-control-sm @error('alamat') is-invalid @enderror" id="alamat"
                            rows="3" name="alamat">{{ $siswa->alamat }}</textarea>
                        @error('alamat')
                        <div class="invalid-error">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="nomor_telp">Nomor Telp</label>
                        <input type="number"
                            class="form-control form-control-sm @error('nomor_telp') is-invalid @enderror"
                            id="nomor_telp" name="nomor_telp" value="{{ $siswa->nomor_telp }}">
                        @error('nomor_telp')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="id_spp">Tahun Bayar</label>
                        <select class="form-control form-control-sm @error('id_spp') is-invalid @enderror" id="id_spp"
                            name="id_spp" value="{{ old('id_spp') }}">
                            <option value="">Tahun Bayar</option>
                            @foreach ($spp as $s)
                            @if($siswa->id_spp == $s->id)
                            <option value="{{ $s->id }}" selected>{{ $s->tahun . ' - ' . $s->nominal}}</option>
                            @else
                            <option value="{{ $s->id }}">{{ $s->tahun . ' - ' . $s->nominal}}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('spp_bulan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('siswa.index') }}" class="btn btn-danger btn-sm">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

@endsection
{{-- end content --}}

@section('js')
<script>
    $(document).ready(function() {
        $('input, textarea').on('keyup', function() {
            $(this).removeClass('is-invalid');
        });
        $('select').on('change', function() {
            $(this).removeClass('is-invalid');
        });
    });
</script>
@endsection
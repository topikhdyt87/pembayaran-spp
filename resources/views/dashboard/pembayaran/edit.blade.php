@extends('layouts.main')

@section('title', 'Entry Pembayaran | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Edit Transaksi Pembayaran</span>
        <h3 class="page-title">Pembayaran</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Edit Transaksi Pembayaran</h6>
            </div>
            <div class="card-body pt-3">

                <form action="{{ route('pembayaran.update', ['id' => $pembayaran->id]) }}" method="post">@csrf
                    @method('put')
                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" class="form-control @error('nisn') is-invalid @enderror" id="nisn"
                            name="nisn" value="{{ $pembayaran->siswa->nisn }}" autocomplete="off" readonly>
                        @error('nisn')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="spp_bulan">SPP Bulan</label>
                        <select class="form-control form-control-sm @error('spp_bulan') is-invalid @enderror"
                            id="spp_bulan" name="spp_bulan" value="{{ old('spp_bulan') }}">
                            <option value="">Spp Bulan</option>
                            <option value="januari">Januari</option>
                            <option value="Februari">Februari</option>
                            <option value="Maret">Maret</option>
                            <option value="April">April</option>
                            <option value="Mei">Mei</option>
                            <option value="Juni">Juni</option>
                            <option value="Juli">Juli</option>
                            <option value="Agustus">Agustus</option>
                            <option value="September">September</option>
                            <option value="Oktober">Oktober</option>
                            <option value="November">November</option>
                            <option value="Desember">Desember</option>
                        </select>
                        @error('spp_bulan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="jumlah_bayar">Jumlah Bayar</label>
                        <input type="number" class="form-control @error('jumlah_bayar') is-invalid @enderror"
                            id="jumlah_bayar" name="jumlah_bayar" value="{{ $pembayaran->jumlah_bayar }}">
                        @error('jumlah_bayar')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('pembayaran.index') }}" type="reset" class="btn btn-danger btn-sm">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>


@endsection
{{-- end content --}}
@section('js')
<script>
    $(function() {
        const sppBulanValue = '{{ $pembayaran->spp_bulan }}';
        //  for checking selected form(sppBulan)
        if(sppBulanValue !== '') {
            $('#spp_bulan').val(sppBulanValue);
        }
        // for checking validation on clicked button reset
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
        $('button[type="reset"]').on('click', function() {
            $('.is-invalid').removeClass('is-invalid')
        })
    });

    // for deleting data
    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeletePembayaran-' + id).submit();
            }
        })
    }
</script>
@endsection
@extends('layouts.main')

@section('title', 'Entry Pembayaran | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Input Transaksi Pembayaran</span>
        <h3 class="page-title">Pembayaran</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Input Trasksaksi Pembayaran</h6>
            </div>
            <div class="card-body pt-3">

                <form action="{{ route('pembayaran.store') }}" method="post">@csrf

                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" class="form-control @error('nisn') is-invalid @enderror" id="nisn"
                            name="nisn" value="{{ old('nisn') }}" autocomplete="off">
                        @error('nisn')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="spp_bulan">SPP Bulan</label>
                        <select class="form-control form-control-sm @error('spp_bulan') is-invalid @enderror"
                            id="spp_bulan" name="spp_bulan" value="{{ old('spp_bulan') }}">
                            <option value="">Spp Bulan</option>
                            <option value="januari">Januari</option>
                            <option value="Februari">Februari</option>
                            <option value="Maret">Maret</option>
                            <option value="April">April</option>
                            <option value="Mei">Mei</option>
                            <option value="Juni">Juni</option>
                            <option value="Juli">Juli</option>
                            <option value="Agustus">Agustus</option>
                            <option value="September">September</option>
                            <option value="Oktober">Oktober</option>
                            <option value="November">November</option>
                            <option value="Desember">Desember</option>
                        </select>
                        @error('spp_bulan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="jumlah_bayar">Jumlah Bayar</label>
                        <input type="number" class="form-control @error('jumlah_bayar') is-invalid @enderror"
                            id="jumlah_bayar" name="jumlah_bayar" value="{{ old('jumlah_bayar') }}">
                        @error('jumlah_bayar')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                </form>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

<div class="row">
    {{-- table pembaran --}}
    <div class="col-12 mb-4">
        <div class="card card-small overflow-auto">
            <div class="card-header border-bottom">
                <h6 class="m-0">Transaksi Pembayaran Terakhir</h6>
            </div>
            <div class="card-body pt-3">
                <table class="table mb-0 text-center">
                    <thead class="bg-light">
                        <tr>
                            <th scope="col" class="border-0">#</th>
                            <th scope="col" class="border-0">Nama Petugas</th>
                            <th scope="col" class="border-0">Nisn</th>
                            <th scope="col" class="border-0">Nama Siswa</th>
                            <th scope="col" class="border-0">Pembayaran Bulan</th>
                            <th scope="col" class="border-0">Kewajiban SPP</th>
                            <th scope="col" class="border-0">Jumlah Bayar</th>
                            <th scope="col" class="border-0">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($pembayaran as $p)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $p->users->name }}</td>
                            <td>{{ $p->siswa->nisn }}</td>
                            <td>{{ $p->siswa->nama }}</td>
                            <td>{{ $p->spp_bulan }}</td>
                            <td>{{  $p->siswa->spp->nominal }}</td>
                            <td>{{ $p->jumlah_bayar }}</td>
                            <td>
                                <a href="{{ route('pembayaran.edit', ['id' => $p->id]) }}"
                                    class="btn btn-primary btn-sm">Edit</a>
                                <form action="{{ route('pembayaran.destroy', ['id' => $p->id]) }}"
                                    id="formDeletePembayaran-{{ $p->id }}" class="d-inline-block" method="post">
                                    @csrf @method('delete')
                                    <button type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteData({{ $p->id }})">
                                        Hapus
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <div>
                    {{-- {{ $spp->links() }} --}}
                </div>
            </div>
        </div>
    </div>
    {{-- end table pembayaran --}}
</div>
@endsection
{{-- end content --}}
@section('js')
<script>
    $(function() {
        const sppBulanValue = '{{ old("spp_bulan") }}';
        //  for checking selected form(sppBulan)
        if(sppBulanValue !== '') 
        {
            $('#spp_bulan').val(sppBulanValue);
        }
        // for checking validation on clicked button reset
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
        $('button[type="reset"]').on('click', function() {
            $('.is-invalid').removeClass('is-invalid')
        })
    });

    // for deleting data
    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeletePembayaran-' + id).submit();
            }
        })
    }
</script>
@endsection
@extends('layouts.main')

@section('title', 'Entry Pembayaran | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Laporan Pembayaran SPP</span>
        <h3 class="page-title">Laporan</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Laporan Pembayaran SPP</h6>
            </div>
            <div class="card-body pt-3">
                <div class="alert alert-warning" role="alert">
                    Buat Laporan Pembayaran SPP Siswa, Semua Data Pembayaran Siswa akan di buat laporannya.
                </div>
                <a href="{{ route('generateLaporan') }}" class="btn btn-secondary btn-sm" target="_blank">Buat Laporan
                    PDF</a>
                <a href="{{ route('generateLaporanExel') }}" class="btn btn-success btn-sm" target="_blank">Buat Laporan
                    Exel</a>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

@endsection
{{-- end content --}}
@section('js')
<script>
</script>
@endsection
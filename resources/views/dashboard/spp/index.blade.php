@extends('layouts.main')


@section('title', 'SPP | Pembayaran SPP')

@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Data SPP</span>
        <h3 class="page-title">SPP</h3>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <!-- Users Stats -->
    <div class="col-12 col-sm-12 col-lg-6 mb-4">
        <div class="card card-small ">
            <div class="card-header border-bottom">
                <h6 class="m-0 d-inline-block">Data Data SPP</h6>
            </div>
            <div class="card-body pt-3 ">
                <form action="{{ route('spp.store') }}" method="POST">@csrf
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input type="number" class="form-control form-control-sm @error('tahun') is-invalid @enderror"
                            id="tahun" name="tahun" value="{{ old('tahun') }}">
                        @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nominal">Nominal</label>
                        <input type="number" class="form-control form-control-sm @error('nominal') is-invalid @enderror"
                            id="nominal" name="nominal" value="{{ old('nominal') }}">
                        @error('nominal')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <button type="reset" class="btn btn-secondary btn-sm">Reset</button>
                </form>
            </div>
        </div>
    </div>
    <!-- End Users Stats -->

    <!-- Users Stats -->
    <div class="col-12 col-sm-12 col-lg-6 mb-4">
        <div class="card card-small" style="min-height: 280px">
            <div class="card-header border-bottom">
                <h6 class="m-0 d-inline-block">Data SPP</h6>
            </div>
            <div class="card-body pt-3 overflow-auto">
                <table class="table mb-0 text-center">
                    <thead class="bg-light">
                        <tr>
                            <th scope="col" class="border-0">#</th>
                            <th scope="col" class="border-0">Tahun</th>
                            <th scope="col" class="border-0">Nominal</th>
                            <th scope="col" class="border-0">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($spp as $s)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $s->tahun }}</td>
                            <td>{{ $s->nominal }}</td>
                            <td>
                                <a href="{{ route('spp.edit', ['id' => $s->id]) }}"
                                    class="btn btn-primary btn-sm">Edit</a>
                                <form action="{{ route('spp.destroy', ['id' => $s->id ]) }}" method="post"
                                    class="d-inline-block" id="formDeleteSpp-{{ $s->id }}">@csrf
                                    @method('delete')
                                    <button type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteData({{ $s->id }})">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="float-right">
                    {{ $spp->links() }}
                </div>

            </div>
        </div>
    </div>
    <!-- End Users Stats -->
</div>
@endsection
@section('js')
<script>
    $(function() {
        // for checking validation on clicked button reset
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
        $('button[type="reset"]').on('click', function() {
            $('input').removeClass('is-invalid');
        });
    });
    // for deleting data
    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeleteSpp-' + id).submit();
            }
        })
    }
</script>
@endsection
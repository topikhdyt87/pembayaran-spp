@extends('layouts.main')


@section('title', 'SPP | Pembayaran SPP')

@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Edit Data Spp</span>
        <h3 class="page-title">SPP</h3>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <!-- form edit data -->
    <div class="col-12 col-sm-12 col-lg-8 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Edit Data SPP</h6>
            </div>
            <div class="card-body pt-3">
                <form action="{{ route('spp.update',['id' => $spp->id]) }}" method="POST">@csrf @method('put')
                    <div class="form-group">
                        <label for="tahun">Tahun</label>
                        <input type="number" class="form-control form-control-sm @error('tahun') is-invalid @enderror"
                            id="tahun" name="tahun" value="{{ $spp->tahun }}">
                        @error('tahun')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nominal">Nominal</label>
                        <input type="number" class="form-control form-control-sm @error('nominal') is-invalid @enderror"
                            id="nominal" name="nominal" value="{{ $spp->nominal  }}">
                        @error('nominal')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('spp.index') }}" class="btn btn-danger btn-sm">kembali</a>
                </form>
            </div>
        </div>
    </div>
    <!-- end form edit data -->

</div>
@endsection


@section('js')
<script>
    $(function() {
        // for remove validation message on keyup
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
    });
</script>
@endsection
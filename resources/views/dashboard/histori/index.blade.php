@extends('layouts.main')

@section('title', 'Entry Pembayaran | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Histori Pembayaran</span>
        <h3 class="page-title">Histori</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Histori Pembayaran</h6>
            </div>
            <div class="card-body pt-3">
                <div class="row">
                    @foreach ($pembayaran as $p)
                    <div class="col-12 col-sm-12 col-lg-4 my-3">
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item active">
                                    <span class="float-right">
                                        <i class="material-icons">done_outline</i>
                                        {{ $p->created_at->format('H:i d M Y') }}
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    <table width="100%">
                                        <tr>
                                            <td class="">Nama</td>
                                            <td class="">:</td>
                                            <td class="">{{ $p->siswa->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kelas</td>
                                            <td>:</td>
                                            <td>{{ $p->siswa->kelas->nama_kelas }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Bayar</td>
                                            <td>:</td>
                                            <td>{{ $p->jumlah_bayar }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kewajiban SPP</td>
                                            <td>:</td>
                                            <td>{{ $p->siswa->spp->nominal }}</td>
                                        </tr>
                                        <tr>
                                            <td>SPP Bulan</td>
                                            <td>:</td>
                                            <td>{{ $p->spp_bulan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Petugas</td>
                                            <td>:</td>
                                            <td>{{ $p->users->name }}</td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

@endsection
{{-- end content --}}
@section('js')
<script>
</script>
@endsection
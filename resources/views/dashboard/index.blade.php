@extends('layouts.main')

@section('title' , 'Dashboard | Pembayaran SPP')

@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Overview</h3>
    </div>
</div>
@endsection

@section('content')
<!-- Small Stats Blocks -->
<div class="row">
    <div class="col-lg col-md-12 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Pembayaran Bulan
                            {{ now()->format('M') }}</span>
                        <h6 class="stats-small__value count my-3">{{ $currentMonthPembayaran }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span
                            class="stats-small__percentage stats-small__percentage--increase font-weight-bold">{{ $percentage }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-1"></canvas>
            </div>
        </div>
    </div>

    <div class="col-lg col-md-12 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Jumlah Siswa</span>
                        <h6 class="stats-small__value count my-3">{{ $jumlahSiswa }}</h6>
                    </div>
                    <div class="stats-small__data">
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-2"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg col-md-12 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Jumlah Kelas</span>
                        <h6 class="stats-small__value count my-3">{{ $jumlahKelas }}</h6>
                    </div>
                    <div class="stats-small__data">
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-3"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">

</div>
<!-- End Small Stats Blocks -->
<div class="row">
    <!-- Users Stats -->
    <div class="col-lg-6 col-md-12 col-sm-12 mb-4">
        <div class="card card-small" style="min-height: 570px">
            <div class="card-header border-bottom">
                <h6 class="m-0">Transaksi Pembayaran Terakhir</h6>
            </div>
            <div class="card-body pt-0">
                {!! $usersChart->container() !!}
                {!! $usersChart->script() !!}
            </div>
        </div>
    </div>
    <!-- End Users Stats -->
    <!-- Users By Device Stats -->
    <div class="col-lg-6 col-md-12 col-sm-12 mb-4">
        <div class="card card-small" style="min-height: 570px">
            <div class="card-header border-bottom">
                <h6 class="m-0">Transaksi Pembayaran Terakhir</h6>
            </div>
            <div class="card-body d-flex py-0">
                <div class="row">
                    @foreach ($pembayaran as $p)
                    <div class="col-12 my-4">
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item active">
                                    <span class="float-right">
                                        <i class="material-icons">done_outline</i>
                                        {{ $p->created_at->format('H:i d M Y') }}
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    <table width="100%">
                                        <tr>
                                            <td class="">Nama</td>
                                            <td class="">:</td>
                                            <td class="">{{ $p->siswa->nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kelas</td>
                                            <td>:</td>
                                            <td>{{ $p->siswa->kelas->nama_kelas }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Bayar</td>
                                            <td>:</td>
                                            <td>{{ $p->jumlah_bayar }}</td>
                                        </tr>
                                        <tr>
                                            <td>Kewajiban SPP</td>
                                            <td>:</td>
                                            <td>{{ $p->siswa->spp->nominal }}</td>
                                        </tr>
                                        <tr>
                                            <td>SPP Bulan</td>
                                            <td>:</td>
                                            <td>{{ $p->spp_bulan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Petugas</td>
                                            <td>:</td>
                                            <td>{{ $p->users->name }}</td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
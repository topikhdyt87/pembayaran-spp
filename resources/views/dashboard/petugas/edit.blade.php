@extends('layouts.main')

@section('title', 'Edit Data Petugas | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Edit Data Petugas</span>
        <h3 class="page-title">Petugas</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Edit Data Petugas</h6>
            </div>
            <div class="card-body pt-3">
                <form action="{{ route('petugas.update', ['id' => $petugas->id]) }}" method="post">@csrf @method('put')

                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror"
                            id="name" name="name" value="{{ $petugas->name }}">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="level">Level</label>
                        <select class="form-control form-control-sm @error('level') is-invalid @enderror" id="level"
                            name="level">
                            <option value="">Pilih Level</option>
                            <option value="admin">Admin</option>
                            <option value="petugas">Petugas</option>
                        </select>
                        @error('level')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">email</label>
                        <input type="email" class="form-control form-control-sm @error('email') is-invalid @enderror"
                            id="email" name="email" value="{{ $petugas->email }}">
                        @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password">Password (Optional)</label>
                        <input type="password"
                            class="form-control form-control-sm @error('password') is-invalid @enderror" id="password"
                            name="password">
                        @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Konfirmasi (Optional)</label>
                        <input type="password"
                            class="form-control form-control-sm @error('password') is-invalid @enderror"
                            id="password_confirmation" name="password_confirmation">
                    </div>
                    <a href="{{ route('petugas.index') }}" class="btn btn-danger float-left">Kembali</a>
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

@endsection
{{-- end content --}}

@section('js')
<script>
    $(function() {
        const level = '{{ $petugas->level }}';
        if(level !== '') {
            $('#level').val(level);
        }

        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
        $('select').on('change', function() {
            $(this).removeClass('is-invalid');
        })
        
    });

    // for deleting data
    function deleteData(id) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Data yang di hapus tidak dapat di kembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus',
                cancelButtonText: 'Kembali',
            }).then((result) => {
                if (result.value) {
                    $('#formDeletePetugas-' + id).submit();
                }
            })
        }
</script>
@endsection
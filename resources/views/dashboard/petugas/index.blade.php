@extends('layouts.main')

@section('title', 'Data Petugas | Pembayaran SPP')

{{-- for header in content --}}
@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Data Petugas</span>
        <h3 class="page-title">Petugas</h3>
    </div>
</div>
@endsection
{{-- end header content --}}

{{-- main content --}}
@section('content')
<div class="row">
    <!-- input transaksi pembayaran -->
    <div class="col-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Data Petugas</h6>
            </div>
            <div class="card-body pt-3">
                <div class="row">
                    <div class="col-12 my-2">
                        <a href="{{ route('petugas.create') }}" class="shadow btn btn-success">Tambah Data Petugas</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 overflow-auto">
                        <table class="table mb-0 text-center">
                            <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">#</th>
                                    <th scope="col" class="border-0">Nama</th>
                                    <th scope="col" class="border-0">Email</th>
                                    <th scope="col" class="border-0">Level</th>
                                    <th scope="col" class="border-0">Dibuat</th>
                                    <th scope="col" class="border-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($petugas as $p)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ $p->email }}</td>
                                    <td>{{ $p->level }}</td>
                                    <td>{{ $p->created_at->format('H:i, d M y') }}</td>
                                    <td>
                                        <a href="{{ route('petugas.edit', ['id' => $p->id]) }}"
                                            class="btn btn-primary">Edit</a>
                                        <form action="{{ route('petugas.destroy', ['id' => $p->id]) }}"
                                            class="d-inline-block" id="formDeletePetugas-{{ $p->id }}" method="post">
                                            @csrf @method('delete')
                                            <button type="button" class="btn btn-danger"
                                                onclick="deleteData({{ $p->id }})">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end of input transaksi pembayaran --}}
</div>

@endsection
{{-- end content --}}

@section('js')
<script>
    // for deleting data
 function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeletePetugas-' + id).submit();
            }
        })
    }
</script>
@endsection
@extends('layouts.main')

@section('title', 'Kelas | Pembayaran SPP')

@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Data Kelas</span>
        <h3 class="page-title">Kelas</h3>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <!-- Users Stats -->
    <div class="col-12 col-sm-12 col-lg-6 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0 d-inline-block">Input Data Kelas</h6>
            </div>
            <div class="card-body pt-3">
                <form action="{{ route('kelas.store') }}" method="post">@csrf
                    <div class="form-group">
                        <label for="nama_kelas">Nama Kelas</label>
                        <input type="text"
                            class="form-control form-control-sm @error('nama_kelas') is-invalid @enderror"
                            id="nama_kelas" name="nama_kelas" value="{{ old('nama_kelas') }}">
                        @error('nama_kelas')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="kompetensi_keahlian">Kompetensi Keahlian</label>
                        <input type="text"
                            class="form-control form-control-sm @error('kompetensi_keahlian') is-invalid @enderror"
                            id="kompetensi_keahlian" name="kompetensi_keahlian"
                            value="{{ old('kompetensi_keahlian') }}">
                        @error('kompetensi_keahlian')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <button type="reset" class="btn btn-secondary btn-sm">Reset</button>
                </form>
            </div>
        </div>
    </div>
    <!-- End Users Stats -->


    <!-- Users Stats -->
    <div class="col-12 col-sm-12 col-lg-6 mb-4">
        <div class="card card-small" style="min-height: 280px;">
            <div class="card-header border-bottom">
                <h6 class="m-0 d-inline-block">Data Kelas</h6>
            </div>
            <div class="card-body pt-3 overflow-auto">
                <table class="table mb-0 text-center">
                    <thead class="bg-light">
                        <tr>
                            <th scope="col" class="border-0">#</th>
                            <th scope="col" class="border-0">Nama Kelas</th>
                            <th scope="col" class="border-0">Kompetensi Keahlian</th>
                            <th scope="col" class="border-0">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kelas as $k)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $k->nama_kelas }}</td>
                            <td>{{ $k->kompetensi_keahlian }}</td>
                            <td>
                                <a href="{{ route('kelas.edit', ['id' => $k->id]) }}"
                                    class="btn btn-primary btn-sm">Edit</a>
                                <form action="{{ route('kelas.destroy', ['id' => $k->id]) }}"
                                    id="formDeleteKelas-{{ $k->id }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteData({{ $k->id }})">
                                        Hapus
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End Users Stats -->
</div>
@endsection

@section('js')
<script>
    $(function() {

        // for checking validation on clicked button reset
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
        $('button[type="reset"]').on('click', function() {
            $('input').removeClass('is-invalid');
        });
    });
    // for deleting data
    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang di hapus tidak dapat di kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Kembali',
        }).then((result) => {
            if (result.value) {
                $('#formDeleteKelas-' + id).submit();
            }
        })
    }
</script>
@endsection
@extends('layouts.main')

@section('title', 'Kelas | Pembayaran SPP')

@section('header')
<div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Edit Data Kelas</span>
        <h3 class="page-title">Kelas</h3>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <!-- Users Stats -->
    <div class="col-12 col-sm-12 col-lg-8 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Edit Data Kelas</h6>
            </div>
            <div class="card-body pt-3">

                <form action="{{ route('kelas.update', ['id' => $kelas->id]) }}" method="post">
                    @csrf @method('put')
                    <div class="form-group">
                        <label for="nama_kelas">Nama Kelas</label>
                        <input type="text"
                            class="form-control form-control-sm @error('nama_kelas') is-invalid @enderror"
                            id="nama_kelas" name="nama_kelas" value="{{ $kelas->nama_kelas }}">
                        @error('nama_kelas')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="kompetensi_keahlian">Kompetensi Keahlian</label>
                        <input type="text"
                            class="form-control form-control-sm @error('kompetensi_keahlian') is-invalid @enderror"
                            id="kompetensi_keahlian" name="kompetensi_keahlian"
                            value="{{ $kelas->kompetensi_keahlian }}">
                        @error('kompetensi_keahlian')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>


                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="{{ route('kelas.index') }}" class="btn btn-danger btn-sm">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    <!-- End Users Stats -->



</div>
@endsection

@section('js')
<script>
    $(function() {
        // for checking validation on clicked button reset
        $('input').on('keyup', function() {
            $(this).removeClass('is-invalid');
        })
    });
    
</script>
@endsection
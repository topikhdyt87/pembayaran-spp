<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="{{ route('dashboard') }}" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;"
                        src="{{ asset('template/images/shards-dashboards-logo.svg') }}" alt="Shards Dashboard">
                    <span class="d-none d-md-inline ml-1">Pembayaran SPP</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <form action="#" class="main-sidebar__search w-100 border-right d-none">
        <div class="input-group input-group-seamless ml-3">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
            <input class="navbar-search form-control" type="text" placeholder="Search for something..."
                aria-label="Search">
        </div>
    </form>

    <div class="nav-wrapper">

        <ul class="nav flex-column">

            <li class="nav-item">
                <a class="nav-link {{ (request()->segment(1) == 'dashboard' && empty(request()->segment(2))) ? 'active' : '' }}"
                    href="{{ route('dashboard') }}">
                    <i class="material-icons">edit</i>
                    <span>Dashboard </span>
                </a>
            </li>

            @if (Auth::user()->level == 'admin')
            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'petugas')? 'active' : '' }}"
                    href="{{ route('petugas.index') }}">
                    <i class="material-icons">emoji_people</i>
                    <span>Data Petugas </span>
                </a>
            </li>
            @endif

            @if (Auth::user()->level == 'admin')
            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'kelas')? 'active' : '' }}"
                    href="{{ route('kelas.index') }}">
                    <i class="material-icons">group</i>
                    <span>Kelas</span>
                </a>
            </li>
            @endif


            @if (Auth::user()->level == 'admin')
            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'spp')? 'active' : '' }}"
                    href="{{ route('spp.index') }}">
                    <i class="material-icons">local_atm</i>
                    <span>SPP</span>
                </a>
            </li>
            @endif


            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'siswa' ) ? 'active' : '' }}"
                    href="{{ route('siswa.index') }}">
                    <i class="material-icons ">sports_kabaddi</i>
                    <span>Siswa</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'pembayaran')? 'active' : '' }}"
                    href="{{ route('pembayaran.index') }}">
                    <i class="material-icons">input</i>
                    <span>Input Pembayaran</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'historiPembayaran' ) ? 'active' : '' }}"
                    href="{{ route('histori') }}">
                    <i class="material-icons ">local_post_office</i>
                    <span>Histori Pembayaran</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{  (request()->segment(2) == 'laporan')? 'active' : '' }}"
                    href="{{ route('laporan') }}">
                    <i class="material-icons">analytics</i>
                    <span>Generate Laporan</span>
                </a>
            </li>

        </ul>
    </div>
</aside>
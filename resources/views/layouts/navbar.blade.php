<!-- Main Navbar -->
<nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
    <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">

    </form>
    <ul class="navbar-nav border-left flex-row ">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="true" aria-expanded="false">
                <img class="user-avatar rounded-circle mr-2" width="40px"
                    src="{{ asset('img/' . Auth::user()->avatar) }}" alt="User Avatar">
                <span class="d-none d-md-inline-block">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-small">
                <a class="dropdown-item" href="{{ route('petugas.show', ['id' => Auth::user()->id]) }}">
                    <i class="material-icons">&#xE7FD;</i> Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item " href="#" data-target="#logoutModal" data-toggle="modal">
                    <i class="material-icons ">&#xE879;</i>logout
                </a>
            </div>
        </li>
    </ul>

    <nav class="nav">
        <a href="#" class="nav-link nav-link-icon toggle-sidebar d-inline d-md-none d-lg-none text-center border-left"
            data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
            <i class="material-icons">&#xE5D2;</i>
        </a>
    </nav>
</nav>
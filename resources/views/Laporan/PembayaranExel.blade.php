<table>
    <thead>
        <tr>
            <th scope="col">Petugas</th>
            <th scope="col">Siswa</th>
            <th scope="col">Kelas</th>
            <th scope="col">SPP Bulan</th>
            <th scope="col">SPP Nominal</th>
            <th scope="col">Jumlah Bayar</th>
            <th scope="col">Tanggal Bayar</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pembayaran as $p)
        <tr>
            <th scope="row">{{ $p->users->name }}</th>
            <td>{{ $p->siswa->nama }}</td>
            <td>{{ $p->siswa->kelas->nama_kelas }}</td>
            <td>{{ $p->spp_bulan }}</td>
            <td>{{ $p->siswa->spp->nominal }}</td>
            <td>{{ $p->jumlah_bayar }}</td>
            <td>{{ $p->created_at->format('d M Y') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
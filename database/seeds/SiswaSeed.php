<?php

use Illuminate\Database\Seeder;
use App\Siswa;

class SiswaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Siswa::insert([
            [
                'nisn' => '112233445566',
                'nis' => '1122334455',
                'nama' => 'Nour Rismawati',
                'id_kelas' => 1,
                'alamat' => 'Kp.pasir Des.pondokaso Cidahu',
                'nomor_telp' => '082120212021',
                'id_spp' => 1,
                'created_at' => now(),
            ],
            [
                'nisn' => '665544332211',
                'nis' => '5544332211',
                'nama' => 'Siti Nuryanti',
                'id_kelas' => 2,
                'alamat' => 'Kp.asgora Des.purwasari cicurug',
                'nomor_telp' => '085487879898',
                'id_spp' => 2,
                'created_at' => now(),
            ]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Kelas;
class KelasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::insert([
            [
                'nama_kelas' => 'XII RPL 1',
                'kompetensi_keahlian' => 'Rekayasa Perangkat Lunak',
                'created_at' => now()
            ],
            [
                'nama_kelas' => 'XII MM 1',
                'kompetensi_keahlian' => 'Rekayasa Perangkat Lunak',
                'created_at' => now(),
            ]
        ]);
    }
}

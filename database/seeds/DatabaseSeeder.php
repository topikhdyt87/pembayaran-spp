<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SppSeed::class);
        $this->call(KelasSeed::class);
        $this->call(SiswaSeed::class);
        $this->call(PembayaranSeed::class);
    }
}

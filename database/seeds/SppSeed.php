<?php

use Illuminate\Database\Seeder;
use App\Spp;
class SppSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Spp::insert([
            [
                'tahun' => '2019',
                'nominal' => 150000,
                'created_at' => now()
            ],
            [
                'tahun' => '2020',
                'nominal' => 200000,
                'created_at' => now()
            ]
        ]);
    }
}

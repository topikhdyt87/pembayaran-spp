<?php

use Illuminate\Database\Seeder;
use App\Pembayaran;

class PembayaranSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pembayaran::insert([
            [
                'id_petugas' => 1,
                'id_siswa' => 1,
                'spp_bulan' => 'Januari',
                'jumlah_bayar' => 150000,
                'created_at' => now(),
            ],
            [
                'id_petugas' => 2,
                'id_siswa' => 2,
                'spp_bulan' => 'Januari',
                'jumlah_bayar' => 200000,
                'created_at' => now(),
            ]
        ]);
    }
}

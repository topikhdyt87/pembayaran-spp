<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([[
            'name' => 'Topik Hidayat',
            'email' => 'admin@admin.com',
            'avatar' => 'default.png',
            'password' => bcrypt('admin'),
            'level' => 'admin',
            'created_at' => now()
        ],
        [
            'name' => 'Sri Indah Agustin',
            'email' => 'petugas@petugas.com',
            'avatar' => 'default.png',
            'password' => bcrypt('petugas'),
            'level' => 'petugas',
            'created_at' => now()
        ]]);
    }
}
